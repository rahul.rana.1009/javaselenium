package framework.features.loginManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.features.navigation.TopMenu;
import framework.pageObjects.loginManagement.LoginPage;
import framework.utility.common.Assertion;
import framework.utility.common.DataFactory;
import framework.utility.common.DriverFactory;
import framework.utility.common.Utils;
import framework.utility.globalConst.ConfigInput;
import framework.utility.globalConst.Constants;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class Login {
    private static ExtentTest pNode;

    public static Login init(ExtentTest t1) throws Exception {
        pNode = t1;
        return new Login();
    }


    public void loginICAdmin() throws Exception {
        login(ConfigInput.siqAdminId, Constants.IC_TITLE_ADMIN_HOME);
    }

    public void loginICFieldFacing() throws Exception {
        login(ConfigInput.siqUserId, Constants.IC_TITLE_IC_HOME);
    }

    /**
     * Login with specific user, if user is laready logged in > return
     *
     * @param loginId
     * @throws IOException
     */
    public void login(String loginId, String homeTitle) throws Exception {
        Markup m = MarkupHelper.createLabel("Login as: " + loginId, ExtentColor.BLUE);
        pNode.info(m);

        User siqUser = DataFactory.getSiqUser(loginId);

        // check if user is already logged in with current View
        if (ConfigInput.lastUser != null && ConfigInput.lastUser.equalsIgnoreCase(siqUser.loginId)) {
            pNode.pass("Already Logged in as User: " + loginId);
            return;
        }

        if (ConfigInput.lastUser != null) {
            logOut();
        }

        try {
            WebDriver driver = DriverFactory.getDriver();
            driver.get(ConfigInput.url);
            pNode.info("Open Application, URL: " + ConfigInput.url);

            LoginPage p1 = LoginPage.init(pNode);
            p1.setUserName(siqUser.loginId);
            p1.setPassword(siqUser.password);
            p1.clickOnSubmit();

            int count = 0;
            while (!driver.getTitle().contains("Home") && count < Constants.EXPLICIT_WAIT_TIME) {
                Thread.sleep(700);
                count++;
            }

            String currentTitle = driver.getTitle();
            pNode.info("Current Page Title: " + currentTitle);

            if (!currentTitle.contains("Home")) {
                if (p1.isSwitchToLightningLinkPresent())
                    p1.clickOnSwitchToLightningMode();
                else if (p1.isTryLightningButtonVisible())
                    p1.clickOnTryLightning();

                count = 0;
                while (!driver.getTitle().contains("Home") && count < Constants.EXPLICIT_WAIT_TIME) {
                    Thread.sleep(1000);
                    count++;
                }
            }

            // verify that Sales IQ is open
            if (Assertion.verifyEqual(p1.selectView(homeTitle),
                    true,
                    "Verify that Sales IQ Lightning is successful Open", pNode)) {
                ConfigInput.lastUser = siqUser.loginId;
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Log out from application, close any additional Tabs
     *
     * @throws Exception
     */
    public void logOut() throws Exception {
        try {
            if (ConfigInput.lastUser == null) {
                pNode.info("No Last User Logged In!");
                return;
            }

            // close CR handle if open
            String mainHandle = Utils.getMainWindowHandle();
            Utils.switchToMainWindow(mainHandle); // close any open window other than Main Window
            TopMenu.init(pNode).clickOnLogout();
            int count = 0;
            while (!DriverFactory.getDriver().getTitle().contains("Login | Salesforce") && count < 8) {
                Thread.sleep(1000);
                count++;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            Utils.captureScreen(pNode);
            ConfigInput.lastUser = null;
            DriverFactory.closeDriver();
        }
    }


}
