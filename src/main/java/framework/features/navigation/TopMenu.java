package framework.features.navigation;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.utility.common.Assertion;
import framework.utility.common.DriverFactory;
import framework.utility.common.Utils;
import framework.utility.globalConst.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class TopMenu extends PageInit {
    @FindBy(css = ".slds-context-bar")
    WebElement containerTopMenu;

    @FindBy(css = ".slds-dropdown.slds-dropdown--right")
    WebElement containerDropDown;

    @FindBy(linkText = "Switch to Salesforce Classic")
    WebElement linkSwitchToClassic;

    @FindBy(linkText = "Log Out")
    WebElement linkLogOut;

    @FindBy(linkText = "Switch to Lightning Experience")
    WebElement linkSwitchToLightningExperience;

    @FindBy(css = "[data-aura-class='forceSocialPhoto']")
    WebElement profileIcon;

    @FindBy(css = "[title='List View']")
    WebElement tabTableView;

    @FindBy(css = "[title='Map View']")
    WebElement tabMapView;

    @FindBy(css = "[placeholder='Search Salesforce']")
    WebElement searchSalesForce;

    @FindBy(css = ".oneAppNavContainer")
    WebElement containerTopMenuV7;

    @FindBy(xpath = "//one-app-nav-bar-menu-button[contains(@class, 'more-button')]")
    WebElement btnMore;


    public void navReports() throws Exception {
        navTopMenu("Reports");
    }

    public void navDataManagement() throws Exception {
        navTopMenu("Data Management");
    }

    public void navPerfAcademy() throws Exception {
        navTopMenu("Performance Academy");
    }

    public void switchToClassicMode() throws Exception {
        if (elementIsDisplayed(linkSwitchToLightningExperience)) {
            pageInfo.info("Classic mode is already ON!");
        } else {
            clickOnElement(profileIcon, "Profile Icon");
            Thread.sleep(Constants.MAX_WAIT_TIME);
            clickOnElement(linkSwitchToClassic, "Switch To Classic");
        }
    }

    public void clickOnLogout() throws Exception {
        if (elementIsDisplayed(profileIcon)) {
            clickOnElement(profileIcon, "Profile Icon");
            clickOnElementUsingJs(linkLogOut, "Logout");
        }
    }

    public void switchToLightningMode() {
        if (!elementIsDisplayed(linkSwitchToLightningExperience)) {
            pageInfo.info("Lightning mode is already ON!");
        } else {
            clickOnElement(linkSwitchToLightningExperience, "Switch to Sales IQ Lightning");
        }
    }

    public void navToWorkSpaces() throws Exception {
        navTopMenu("Workspaces");
    }

    public void navToAlignment() throws Exception {
        navTopMenu("Alignments");
        Thread.sleep(Constants.MAX_WAIT_TIME);
    }

    /**
     * get all links in oneAppNavContainer, find the linkName > navigate to the same, wiat for page load and return
     * @param linkName
     * @throws Exception
     */
    public void navTopMenu(String linkName) throws Exception {
        List<WebElement> links = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".oneAppNavContainer")))
                .findElements(By.xpath(".//a"));

        for (WebElement elem : links) {
            if (elem.getAttribute("title").equalsIgnoreCase(linkName)) {
                // get the current URL and Append the Link URL
                DriverFactory.getDriver().get(elem.getAttribute("href"));
                break;
            }
        }

        int count = 0;
        while (!DriverFactory.getCurrentTitle().contains(linkName) && count < Constants.EXPLICIT_WAIT_TIME) {
            Thread.sleep(1200);
            count++;
        }

        if (DriverFactory.getCurrentTitle().contains(linkName)){
            pageInfo.pass("successfully Navigated to: " + linkName);
            Utils.switchToContentFrame();
        }
        else
            Assertion.markAsFailure("Couldn't find the Link :" + linkName);

    }

    public TopMenu(ExtentTest t1) {
        super(t1);
    }

    public static TopMenu init(ExtentTest t1) {
        Utils.switchToDefaultContent();
        return new TopMenu(t1);
    }
}
