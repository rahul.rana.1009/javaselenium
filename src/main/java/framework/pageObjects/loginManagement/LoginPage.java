package framework.pageObjects.loginManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.utility.common.Assertion;
import framework.utility.common.DriverFactory;
import framework.utility.common.Utils;
import framework.utility.globalConst.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends PageInit {

    @FindBy(id = "username")
    WebElement txtUserName;

    @FindBy(id = "lexTryNow")
    WebElement btnTryLightning;

    @FindBy(id = "password")
    WebElement txtPassWord;

    @FindBy(css = "[id='Login'][type='submit']")
    WebElement btnSignIn;

    @FindBy(xpath = "//*[@id='oneHeader']//nav//button[1]")
    WebElement btnSlds;

    @FindBy(linkText = "Switch to Lightning Experience")
    WebElement linkSwitchToLightningExperience;


    public boolean isTryLightningButtonVisible() {
        return elementIsClickable(btnTryLightning);
    }

    public void clickOnTryLightning() throws Exception {
        clickOnElementUsingJs(btnTryLightning, "Try Lightning");
        Thread.sleep(Constants.MAX_WAIT_TIME);
    }

    public boolean isSwitchToLightningLinkPresent() throws Exception {
        Utils.captureScreen(pageInfo);
        return elementIsClickable(linkSwitchToLightningExperience);
    }

    public void clickOnSwitchToLightningMode() throws Exception {
        Thread.sleep(Constants.MAX_WAIT_TIME);
        clickOnElement(linkSwitchToLightningExperience, "linkSwitchToLightningExperience");
        Thread.sleep(Constants.MAX_WAIT_TIME);
    }

    public LoginPage setUserName(String text) {
        setText(txtUserName, text, "UserName");
        return this;
    }

    public LoginPage setPassword(String text) {
        setText(txtPassWord, text, "Password");
        return this;
    }

    public LoginPage clickOnSubmit() throws Exception {
        clickOnElement(btnSignIn, "Sign In");
        Thread.sleep(Constants.MAX_WAIT_TIME);
        return this;
    }

    public boolean selectView(String view) {

        try {
            if (DriverFactory.getCurrentTitle().equalsIgnoreCase(view)) {
                return true;
            } else {
                clickOnElement(btnSlds, "Menu");
                WebElement elem = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".panel-content.scrollable")))
                        .findElement(By.xpath(".//lightning-button[1]/button"));

                clickOnElementUsingJs(elem, "View All");
                String linkName = view.equalsIgnoreCase(Constants.IC_TITLE_IC_HOME) ? Constants.IC_VIEW_FIELD : Constants.IC_VIEW_ADMIN;
                WebElement eView = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-name='" + linkName + "']")));
                clickOnElementUsingJs(eView, view);

                int count = 0;
                while (!driver.getTitle().contains(view) && count < Constants.EXPLICIT_WAIT_TIME) {
                    Thread.sleep(1000);
                    count++;
                }

                // re check the current view
                if (DriverFactory.getDriver().getTitle().equalsIgnoreCase(view))
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            pageInfo.error(e.getMessage());
            Assertion.markAsFailure(e.getMessage());
        }

        return false;
    }


    public LoginPage(ExtentTest t1) {
        super(t1);
    }

    public static LoginPage init(ExtentTest t1) {
        return new LoginPage(t1);
    }

}
