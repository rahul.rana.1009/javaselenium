package framework.pageObjects.reports;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.utility.common.Assertion;
import framework.utility.common.DriverFactory;
import framework.utility.common.Utils;
import framework.utility.globalConst.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Reports_Pg1 extends PageInit {

    @FindBy(css = "[value='Apply Filter'][type='button']")
    WebElement btnApplyFilter;

    public boolean isApplyButtonAvailable(){
        return elementIsDisplayed(btnApplyFilter);
    }

    public void clickOnApplyFilter(){
        clickOnElement(btnApplyFilter, "btnApplyFilter");
    }

    public Reports_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static Reports_Pg1 init(ExtentTest t1) {
        return new Reports_Pg1(t1);
    }

}
