package framework.utility.common;

import framework.utility.globalConst.FilePath;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class ExcelUtils {

    private static Logger logger = LoggerFactory.getLogger(ExcelUtils.class);

    /**
     * Get Table as Array - use this method for creating a Data Provider
     *
     * @param FilePath  - TestData file path
     * @param sheetName - 0 for Sheet 1
     * @return
     * @throws Exception
     */
    public static Object[][] getTableArray(String FilePath, int sheetName) throws Exception {
        DataFormatter formatter = new DataFormatter();
        String[][] tabArray = null;

        try {

            FileInputStream inp = new FileInputStream(FilePath);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(sheetName);
            int startRow = 1;
            int startCol = 0;
            int ci, cj;

            // get total roles
            int totalRows = sheet.getLastRowNum();
            Row r = sheet.getRow(1);

            if (r == null) {
                logger.info("The target Excel Sheet is Empty");
                return null;
            }

            // get total number of columns
            int totalCols = r.getLastCellNum();

            tabArray = new String[totalRows][totalCols];
            ci = 0;
            for (int i = startRow; i <= totalRows; i++, ci++) {
                cj = 0;
                for (int j = startCol; j < totalCols; j++, cj++) {
                    Cell cell = sheet.getRow(i).getCell(j);
                    if (cell == null) {
                        tabArray[ci][cj] = "";
                    } else {
                        tabArray[ci][cj] = formatter.formatCellValue(cell);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Could not read the Excel sheet");
            e.printStackTrace();
        }

        return (tabArray);
    }

    public static void writeDataToExcel(Sheet sheet, int iRow, int iColumn, String text) {
        Row row = sheet.getRow(iRow);
        if (row == null) {
            sheet.createRow(iRow);
        }

        Cell cell = sheet.getRow(iRow).getCell(iColumn);
        if (cell == null) {
            sheet.getRow(iRow).createCell(iColumn);
            cell = sheet.getRow(iRow).getCell(iColumn);
        }

        cell.setCellValue("" + text);
    }

    /**
     * Delete a Excel FIle
     *
     * @param filePath
     */
    public static void deleteExcel(String filePath) {

        File file = new File(filePath);
        if (file.delete()) {
            logger.info(file.getName() + " is deleted!");
        } else {
            logger.info("Delete operation is failed.");
        }

    }

    public static void clearSheet(String fileName, int sheetIndex, String newSheetName) throws Exception {
        FileInputStream inp = new FileInputStream(new File(fileName));
        Workbook wb = WorkbookFactory.create(inp);
        wb.removeSheetAt(sheetIndex);
        wb.createSheet(newSheetName);
        wb.setSheetOrder(newSheetName, sheetIndex);
        FileOutputStream output = new FileOutputStream(new File(fileName));
        wb.write(output);
        output.close();
    }

    /**
     * Write Header
     *
     * @param filePath   : Path to the file
     * @param headerList List of the header of the File
     */
    private static void writeHeader(String filePath, String[] headerList, int sheetIndex) throws IOException {
        FileOutputStream fileOut = null;
        InputStream inp = null;

        try {
            inp = new FileInputStream(new File(filePath));
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(sheetIndex);

            for (int i = 0; i < headerList.length; i++) {
                writeDataToExcel(sheet, 0, i, headerList[i].toString().trim());
            }

            fileOut = new FileOutputStream(new File(filePath));
            wb.write(fileOut);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.markAsFailure("Failed to Write the Workspace to Excel");
        } finally {
            if (fileOut != null)
                fileOut.close();
        }
    }

}