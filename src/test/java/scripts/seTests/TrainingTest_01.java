package scripts.seTests;


import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.Assertion;
import framework.utility.common.DriverFactory;
import framework.utility.common.Utils;
import org.testng.annotations.Test;
import scripts.TestInit;

public class TrainingTest_01 extends TestInit {

    @Test(priority = 1, groups = {"SET_1"}, enabled = true)
    public void TC_FEATURE_01() {
        ExtentTest t1 = pNode.createNode("TC_FEATURE_01", "TC_FEATURE_01");
        try {
            System.out.println("test1");
            DriverFactory.getDriver().get("https://google.com");
            Thread.sleep(2000);
            Utils.captureScreen(t1);
            //11c5b8a99959890a22c59402730d327280
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {"SET_2"})
    public void TC_FEATURE_02() {
        ExtentTest t1 = pNode.createNode("TC_FEATURE_02",
                "TC_FEATURE_02");
        try {
            System.out.println("test2");
            DriverFactory.getDriver().get("https://facebook.com");
            Thread.sleep(2000);
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {"SET_3"})
    public void TC_FEATURE_03() {
        ExtentTest t1 = pNode.createNode("TC_FEATURE_03", "TC_FEATURE_03");
        try {
            System.out.println("test3");
            DriverFactory.getDriver().get("https://app.crossbrowsertesting.com");
            Thread.sleep(2000);
            Utils.captureScreen(t1);


            /*String devKey = "0812b35c0539f0262bce2da65efdd09c";
            URL url = new URL("http://localhost/testlink/lib/api/xmlrpc/v1/xmlrpc.php");
            TestLinkAPI api = new TestLinkAPI(url, devKey);
            ExecutionStatus status = new ExecutionStatus("PASS");

            api.reportTCResult(4,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "");*/
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
